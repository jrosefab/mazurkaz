/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';
import {I18n} from 'aws-amplify';
import Router from './src/navigation/Router';
import {withAuthenticator, AmplifyTheme} from 'aws-amplify-react-native';

I18n.setLanguage('fr');

const authTheme = {
  ...AmplifyTheme,
  button: {
    ...AmplifyTheme.button,
    backgroundColor: '#3BC0AF',
    borderRadius: 60,
  },
  input: {
    ...AmplifyTheme.input,
    borderRadius: 60,
  },
  phoneInput: {
    ...AmplifyTheme.phoneInput,
    borderRadius: 60,
  },
  sectionFooterLink: {
    ...AmplifyTheme.sectionFooterLink,
    color: '#000',
  },
  buttonDisabled: {
    ...AmplifyTheme.buttonDisabled,
    borderRadius: 60,
    backgroundColor: '#6ec2b7',
  },
};

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="default" />
      <Router />
    </>
  );
};

export default withAuthenticator(App, false, [], null, authTheme);
