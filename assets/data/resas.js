export default [
  {
    id: '0',
    image:
      'https://q-xx.bstatic.com/images/hotel/max1024x768/235/235026439.jpg',
    type: 'Semi Luxueux',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 65,
    totalPrice: 390,
    coordinate: {
      latitude: 14.6011689,
      longitude: -61.0674276,
    },
  },
  {
    id: '1',
    image:
      'https://antilleslocation.com/gestion/images/logement/Appartement-location-Martinique-Saint-Joseph-34894-3158.jpg',
    type: "Chambre d'hôte",
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 48,
    totalPrice: 390,
    coordinate: {
      latitude: 14.55704,
      longitude: -61.05336,
    },
  },
  {
    id: '2',
    image: 'https://images.fnaim.fr/images1/img19/913019L40000218T01.jpg',
    type: 'Maison de courée',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 25,
    totalPrice: 390,
    coordinate: {
      latitude: 14.489468563588893,
      longitude: -61.03690101902681,
    },
  },
  {
    id: '3',
    image: 'https://q-xx.bstatic.com/images/hotel/max1024x768/786/78679891.jpg',
    type: 'Maison au calme en campagne',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 120,
    totalPrice: 390,
    coordinate: {
      latitude: 14.757526990107166,
      longitude: -60.91967637281521,
    },
  },
  {
    id: '4',
    image: 'https://cf.bstatic.com/images/hotel/max1024x768/151/151104556.jpg',
    type: 'Appartement près de la plage',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 68,
    totalPrice: 390,
    coordinate: {
      latitude: 14.709673107607598,
      longitude: -61.16955790359129,
    },
  },
  {
    id: '5',
    image:
      'https://www.meretdemeures.com/media/CACHE/images/uploads/33be8ba0-57b5-40c5-9337-e9d55ecb9dff/8fadeff9c32ceb921fcac04040260313.JPG',
    type: 'Appartement non fumeur',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 88,
    totalPrice: 390,
    coordinate: {
      latitude: 14.672817333168748,
      longitude: -61.03883799488337,
    },
  },
  {
    id: '6',
    image:
      'https://cf.bstatic.com/xdata/images/hotel/max1024x768/151104550.webp?k=0633df04c18404bc7844f63c86ed4c87e6bf4ee72a34a8a424f1bc9e694f1a0b&o=',
    type: 'Maison mitoyenne',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 74,
    totalPrice: 390,
    coordinate: {
      latitude: 14.641278630421212,
      longitude: -61.02387126918482,
    },
  },
  {
    id: '7',
    image:
      'https://www.e-monsite.com/photos/villa-patou-rzxnw0w63n5i6120ho11.jpg',
    type: 'Maison avec piscine',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 40,
    totalPrice: 390,
    coordinate: {
      latitude: 14.623132566889268,
      longitude: -61.06688495345427,
    },
  },
  {
    id: '8',
    image:
      'https://img.pixers.pics/pho_wat(s3:700/FO/56/34/18/700_FO563418_5d6dab7d79eb00c02b8ff410e15c4b08.jpg,700,525,cms:2018/10/5bd1b6b8d04b8_220x50-watermark.png,over,480,475,jpg)/stickers-maison-antillaise.jpg.jpg',
    type: 'Chalet',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 20,
    totalPrice: 390,
    coordinate: {
      latitude: 14.647740842774626,
      longitude: -61.09943766108048,
    },
  },
  {
    id: '9',
    image:
      'https://media.gettyimages.com/photos/yellow-wooden-house-with-curtains-blowing-out-the-door-corossol-st-picture-id55843097?s=612x612',
    type: 'Havre de paix',
    longDescription:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    title: 'New lux apartement in the center of santa',
    bed: 3,
    bedroom: 2,
    oldPrice: 76,
    newPrice: 300,
    totalPrice: 390,
    coordinate: {
      latitude: 14.7788733210568,
      longitude: -61.17470082828281,
    },
  },
];
