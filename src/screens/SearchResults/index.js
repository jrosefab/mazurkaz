import React, {useEffect, useState} from 'react';
import {View, FlatList} from 'react-native';
import Post from '../../components/Post';

export const SearchResultsScreen = (props) => {
  const {posts} = props;

  return (
    <View style={{backgroundColor: '#fff', height: '100%'}}>
      <FlatList data={posts} renderItem={({item}) => <Post post={item} />} />
    </View>
  );
};

export default SearchResultsScreen;
