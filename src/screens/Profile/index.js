import React from 'react';
import {Auth} from 'aws-amplify';
import {View, Text, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';

export const ProfileScreen = () => {
  const logout = () => {
    Auth.signOut();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.logout} onPress={logout}>
        <AntDesign name="logout" size={25} color="#fff" />
        <Text style={{color: '#fff'}}>Se déconnecter</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ProfileScreen;
