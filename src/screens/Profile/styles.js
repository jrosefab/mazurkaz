import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: 'white',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  logout: {
    backgroundColor: '#FF6666',
    width: '80%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    color: '#fff',
    padding: 20,
    borderRadius: 60,
    fontWeight: 'bold',
  },
});

export default styles;
