import React, {useState, useEffect, useRef} from 'react';
import {View, FlatList} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import MarkerMaps from '../../components/MarkerMaps';
import PostCarousel from '../../components/PostCarousel';

export const MapResultsScreen = (props) => {
  const [selectedPlaceId, setSelectedPlaceId] = useState(null);
  const {posts} = props;

  const flatlist = useRef();
  const map = useRef();

  const viewConfig = useRef({itemVisiblePercentThreshold: 70});

  const onViewChanged = useRef(({viewableItems}) => {
    if (viewableItems.length > 0) {
      const selectedPlace = viewableItems[0].item;
      setSelectedPlaceId(selectedPlace.id);
    }
  });

  useEffect(() => {
    if (!selectedPlaceId || !flatlist) {
      return;
    }
    const index = posts.findIndex((place) => place.id === selectedPlaceId);
    flatlist.current.scrollToIndex({index});

    const seletedPlace = posts[index];
    const region = {
      latitude: seletedPlace.latitude,
      longitude: seletedPlace.longitude,
      latitudeDelta: 0.3,
      longitudeDelta: 0.09,
    };
    map.current.animateToRegion(region);
  }, [selectedPlaceId]);

  return (
    <View style={{width: '100%', height: '100%'}}>
      <MapView
        ref={map}
        style={{width: '100%', height: '100%'}}
        provider={PROVIDER_GOOGLE}
        annotations={{subtitle: 'ici'}}
        initialRegion={{
          latitude: 14.6011689,
          longitude: -61.0674276,
          latitudeDelta: 0.04,
          longitudeDelta: 0.05,
        }}>
        {posts.map((place) => (
          <MarkerMaps
            type={place.type}
            onPress={() => setSelectedPlaceId(place.id)}
            isSelected={place.id === selectedPlaceId}
            key={place.id}
            coordinate={{latitude: place.latitude, longitude: place.longitude}}
            price={place.newPrice}
          />
        ))}
      </MapView>
      <View style={{position: 'absolute', bottom: 20}}>
        <FlatList
          ref={flatlist}
          snapToAlignment={'center'}
          decelerationRate={'fast'}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={posts}
          renderItem={({item}) => <PostCarousel post={item} />}
          viewabilityConfig={viewConfig.current}
          onViewableItemsChanged={onViewChanged.current}
        />
      </View>
    </View>
  );
};

export default MapResultsScreen;
