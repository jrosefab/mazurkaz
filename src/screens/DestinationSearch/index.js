import React, {useState} from 'react';
import {View, FlatList, TextInput, Text, Pressable} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import SuggestionRow from '../../components/SuggestionRow';
import searchResults from '../../../assets/data/search';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

export const DestinationScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <GooglePlacesAutocomplete
        placeholder="Choisir une commune ?"
        onPress={(data, details = null) => {
          navigation.navigate('guests', {viewport: details.geometry.viewport});
        }}
        fetchDetails
        styles={{
          textInput: styles.textInput,
        }}
        query={{
          key: 'AIzaSyAR2QibjsQO3PjEZhsjPd7xbB-h09CdTv4',
          language: 'fr',
          types: '(cities)',
          components: 'country:mq',
        }}
        enablePoweredByContainer={false}
        suppressDefaultStyles
        renderRow={(item) => <SuggestionRow item={item} />}
      />
    </View>
  );
};

export default DestinationScreen;
