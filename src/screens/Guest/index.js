import React, {useState} from 'react';
import {View, Text, Pressable} from 'react-native';
import {useNavigation, useRout} from '@react-navigation/native';
import {useRoute} from '@react-navigation/native';
import styles from './styles';

export const GuestScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const [adults, setAdults] = useState(0);
  const [children, setChildren] = useState(0);
  const [babies, setBabies] = useState(0);

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.row}>
          <View>
            <Text style={{fontWeight: 'bold'}}>Adulte</Text>
            <Text style={{color: '#5b5b5b'}}>À partir de 14 ans</Text>
          </View>
          <View style={styles.counter}>
            <Pressable
              onPress={() => setAdults(Math.max(0, adults - 1))}
              style={styles.button}>
              <Text style={{fontSize: 20}}>-</Text>
            </Pressable>
            <Text style={{fontSize: 18, marginHorizontal: 20}}>{adults}</Text>
            <Pressable
              onPress={() => setAdults(adults + 1)}
              style={styles.button}>
              <Text style={{fontSize: 20}}>+</Text>
            </Pressable>
          </View>
        </View>

        <View style={styles.row}>
          <View>
            <Text style={{fontWeight: 'bold'}}>Enfants</Text>
            <Text style={{color: '#5b5b5b'}}>De 3 à 14 ans</Text>
          </View>
          <View style={styles.counter}>
            <Pressable
              onPress={() => setChildren(Math.max(0, children - 1))}
              style={styles.button}>
              <Text style={{fontSize: 20}}>-</Text>
            </Pressable>
            <Text style={{fontSize: 18, marginHorizontal: 20}}>{children}</Text>
            <Pressable
              onPress={() => setChildren(children + 1)}
              style={styles.button}>
              <Text style={{fontSize: 20}}>+</Text>
            </Pressable>
          </View>
        </View>

        <View style={styles.row}>
          <View>
            <Text style={{fontWeight: 'bold'}}>Bébés</Text>
            <Text style={{color: '#5b5b5b'}}>Moins de 3 ans</Text>
          </View>
          <View style={styles.counter}>
            <Pressable
              onPress={() => setBabies(Math.max(0, babies - 1))}
              style={styles.button}>
              <Text style={{fontSize: 20}}>-</Text>
            </Pressable>
            <Text style={{fontSize: 18, marginHorizontal: 20}}>{babies}</Text>
            <Pressable
              onPress={() => setBabies(babies + 1)}
              style={styles.button}>
              <Text style={{fontSize: 20}}>+</Text>
            </Pressable>
          </View>
        </View>
      </View>

      <Pressable
        style={styles.action}
        onPress={() =>
          navigation.navigate('home', {
            screen: 'discover',
            params: {
              screen: 'search_results',
              params: {
                guests: adults + children,
                viewport: route.params.viewport,
              },
            },
          })
        }>
        <Text style={{color: '#fff'}}>Rechercher</Text>
      </Pressable>
    </View>
  );
};

export default GuestScreen;
