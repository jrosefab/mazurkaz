import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    height: '100%',
    backgroundColor: '#fff',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    borderColor: 'lightgrey',
  },
  counter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    borderRadius: 30,
    width: 45,
    height: 45,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#5b5b5b',
  },
  action: {
    marginBottom: 20,
    backgroundColor: '#3BC0AF',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginHorizontal: 20,
    borderRadius: 60,
  },
});

export default styles;
