import React, {useRef, useState} from 'react';
import {View, Text, FlatList, ImageBackground, Pressable} from 'react-native';
import PostCarousel from '../../components/PostCarousel';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Foundation from 'react-native-vector-icons/Foundation';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import resas from '../../../assets/data/resas';
import {ScrollView} from 'react-native-gesture-handler';

export const HomeScreen = () => {
  const navigation = useNavigation();
  const [selectedPlaceId, setSelectedPlaceId] = useState(null);
  const flatlist = useRef();
  const map = useRef();
  const viewConfig = useRef({itemVisiblePercentThreshold: 70});
  const onViewChanged = useRef(({viewableItems}) => {
    if (viewableItems.length > 0) {
      const selectedPlace = viewableItems[0].item;
      setSelectedPlaceId(selectedPlace.id);
    }
  });

  return (
    <ScrollView style={{backgroundColor: '#fff'}}>
      <ImageBackground
        source={require('../../../assets/images/beach.jpg')}
        style={styles.image}>
        <Pressable
          style={styles.searchButton}
          onPress={() => navigation.navigate('destination')}>
          <Text style={styles.searchButtonText}>Dans quelle commune ?</Text>
          <Feather name="search" size={25} color={'#3BC0AF'} />
        </Pressable>
        <Text style={styles.title}>Au{'\n'}Pays</Text>
        <Pressable
          style={styles.button}
          onPress={() => console.warn('Explore')}>
          <Text style={styles.buttonText}>Envie de conseil ?</Text>
        </Pressable>
      </ImageBackground>
      <View>
        <Text style={styles.assetsText}>Les plus vues</Text>
        <FlatList
          ref={flatlist}
          snapToAlignment={'center'}
          decelerationRate={'fast'}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={resas}
          renderItem={({item}) => <PostCarousel post={item} />}
          viewabilityConfig={viewConfig.current}
          onViewableItemsChanged={onViewChanged.current}
        />
      </View>
      <View style={{marginBottom: 60, marginTop: 30}}>
        <Text style={styles.assetsText}>Des préférences ?</Text>
        <View style={styles.preferences}>
          <View style={styles.preference}>
            <MaterialCommunityIcons
              name="castle"
              style={styles.icon}
              size={50}
            />
            <Text style={styles.preferenceText}>Un domaine</Text>
          </View>
          <View style={styles.preference}>
            <MaterialCommunityIcons
              name="home-modern"
              style={styles.icon}
              size={50}
            />
            <Text style={styles.preferenceText}>Loft</Text>
          </View>
          <View style={styles.preference}>
            <FontAwesome5 name="swimming-pool" style={styles.icon} size={50} />
            <Text style={styles.preferenceText}>Avec piscine</Text>
          </View>
          <View style={styles.preference}>
            <MaterialCommunityIcons
              name="city-variant-outline"
              color={'#28bcfc'}
              style={styles.icon}
              size={50}
            />
            <Text style={styles.preferenceText}>En ville</Text>
          </View>
          <View style={styles.preference}>
            <Foundation name="trees" style={styles.icon} size={50} />
            <Text style={styles.preferenceText}>Campagne</Text>
          </View>
          <View style={styles.preference}>
            <FontAwesome5 name="umbrella-beach" style={styles.icon} size={50} />
            <Text style={styles.preferenceText}>Bord de mer</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default HomeScreen;
