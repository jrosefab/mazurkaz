import React from 'react';
import {View} from 'react-native';
import {useRoute} from '@react-navigation/native';
import places from '../../../assets/data/resas';
import DetailedPost from '../../components/DetailedPost';

export const PostSingleScreen = () => {
  const route = useRoute();
  const post = places.find((place) => place.id === route.params.postId);
  console.log(route.params);
  return (
    <View
      style={{
        backgroundColor: '#fff',
        height: '100%',
      }}>
      <DetailedPost post={post} />
    </View>
  );
};

export default PostSingleScreen;
