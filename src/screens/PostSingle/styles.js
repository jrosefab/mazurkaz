import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 500,
    resizeMode: 'cover',
    justifyContent: 'center',
    zIndex: 1,
  },
  searchButton: {
    zIndex: 2,
    width: Dimensions.get('screen').width - 30,
    marginHorizontal: 20,
    top: 20,
    position: 'absolute',
    height: 50,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginLeft: 25,
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 100,
  },
  searchButtonText: {
    textAlign: 'center',
    color: '#3BC0AF',
    fontWeight: 'bold',
  },
  title: {
    fontSize: 80,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
  },
  button: {
    width: 200,
    height: 50,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 25,
    alignItems: 'center',
    borderColor: '#fff',
    borderWidth: 4,
    backgroundColor: 'transparent',
    borderRadius: 100,
    marginTop: 25,
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default styles;
