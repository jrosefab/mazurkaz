import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import DestinationSearch from '../screens/DestinationSearch';
import BottomTabNavigator from '../navigation/BottomTabNavigator';
import GuestScreen from '../screens/Guest';
import PostSingleScreen from '../screens/PostSingle';

const {Navigator, Screen} = createStackNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Navigator>
        <Screen
          name="home"
          component={BottomTabNavigator}
          options={{
            headerShown: false,
          }}
        />
        <Screen
          name="destination"
          component={DestinationSearch}
          options={{title: 'Nouvelle destination'}}
        />
        <Screen
          name="guests"
          component={GuestScreen}
          options={{title: 'Vous êtes combien ?'}}
        />
        <Screen
          name="post"
          component={PostSingleScreen}
          options={{title: "Détails de l'annonce"}}
        />
      </Navigator>
    </NavigationContainer>
  );
}
