import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/Home';
import ResultsTabNavigator from './ResultsTabNavigator';

const {Navigator, Screen} = createStackNavigator();

const DiscoverNavigator = () => {
  return (
    <Navigator>
      <Screen
        name="discover"
        component={HomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <Screen
        name="search_results"
        component={ResultsTabNavigator}
        options={{
          title: 'Découvrir',
        }}
      />
    </Navigator>
  );
};

export default DiscoverNavigator;
