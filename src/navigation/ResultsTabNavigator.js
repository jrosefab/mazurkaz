import React, {useEffect, useState} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import SearchResultsScren from '../screens/SearchResults/';
import MapResultsScreen from '../screens/MapResults/';
import {API, graphqlOperation} from 'aws-amplify';
import {listPosts} from '../../graphql/queries';
import {useRoute} from '@react-navigation/native';
const {Navigator, Screen} = createMaterialTopTabNavigator();

const ResultsTabNavigator = () => {
  const [posts, setPosts] = useState([]);
  const route = useRoute();
  const {guests, viewport} = route.params;
  useEffect(() => {
    const getPosts = async () => {
      try {
        const results = await API.graphql(
          graphqlOperation(listPosts, {
            filter: {
              and: {
                maxGuests: {
                  ge: guests,
                },
                latitude: {
                  between: [viewport.southwest.lat, viewport.northeast.lat],
                },
                longitude: {
                  between: [viewport.southwest.lng, viewport.northeast.lng],
                },
              },
            },
          }),
        );
        console.log(results.data.listPosts);
        setPosts(results.data.listPosts.items);
      } catch (e) {
        console.log(e);
      }
    };
    getPosts();
  }, []);

  return (
    <Navigator
      tabBarOptions={{
        activeTintColor: '#3BC0AF',
        indicatorStyle: {
          backgroundColor: '#3BC0AF',
        },
      }}>
      <Screen name="list" options={{tabBarLabel: 'Liste'}}>
        {() => <SearchResultsScren posts={posts} />}
      </Screen>
      <Screen name="map" options={{tabBarLabel: 'Carte'}}>
        {() => <MapResultsScreen posts={posts} />}
      </Screen>
    </Navigator>
  );
};

export default ResultsTabNavigator;
