import React from 'react';
import {View, StyleSheet} from 'react-native';
import {createIconSetFromFontello} from 'react-native-vector-icons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import DiscoverNavigator from './DiscoverNavigator';
import HomeScreen from '../screens/Home';
import ProfileScreen from '../screens/Profile';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import fontelloConfig from '../config.json';
const Logo = createIconSetFromFontello(fontelloConfig);

const {Navigator, Screen} = createBottomTabNavigator();
const BottomTabNavigator = () => {
  return (
    <Navigator
      tabBarOptions={{
        activeTintColor: '#3BC0AF',
        style: {
          height: 65,
          paddingBottom: 10,
        },
      }}>
      <Screen
        name="discover"
        component={DiscoverNavigator}
        options={{
          tabBarIcon: ({color}) => (
            <Feather name="search" size={25} color={color} />
          ),
          tabBarLabel: 'Découvrir',
        }}
      />
      <Screen
        name="saving"
        component={HomeScreen}
        options={{
          tabBarIcon: ({color}) => (
            <Feather name="heart" size={25} color={color} />
          ),
          tabBarLabel: 'Sauvegardé',
        }}
      />
      <Screen
        name="exclusivity"
        component={HomeScreen}
        options={{
          tabBarIcon: ({color}) => (
            <View style={styles.mainIcon}>
              <Logo name="mazurkaz" size={50} color="#fff" />
            </View>
          ),
          tabBarLabel: '',
        }}
      />
      <Screen
        name="messagery"
        component={HomeScreen}
        options={{
          tabBarIcon: ({color}) => (
            <Ionicons name="chatbox-outline" size={25} color={color} />
          ),
          tabBarLabel: 'Messagerie',
        }}
      />
      <Screen
        name="profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({color}) => (
            <FontAwesome name="user-o" size={25} color={color} />
          ),
          tabBarLabel: 'Profil',
        }}
      />
    </Navigator>
  );
};

const styles = StyleSheet.create({
  mainIcon: {
    height: 80,
    marginBottom: 25,
    backgroundColor: '#3BC0AF',
    borderRadius: 80,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
});

export default BottomTabNavigator;
