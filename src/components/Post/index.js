import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

const days = 7;
const Post = (props) => {
  const post = props.post;
  const navigation = useNavigation();

  const goToPostPage = () => {
    navigation.navigate('post', {postId: post.id});
  };

  return (
    <Pressable onPress={goToPostPage} style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri: post.image,
        }}
      />
      <Text style={styles.bedrooms}>
        {post.bed} lit {post.bedroom} chambre
      </Text>
      <Text style={styles.description} numberOfLines={2}>
        {post.type}
      </Text>
      <Text style={styles.prices}>
        <Text style={styles.oldPrice}>{post.oldPrice}€ </Text>
        <Text style={styles.newPrice}> {post.newPrice}€</Text> / nuit
      </Text>
      <Text style={styles.totalPrice}>Total de {post.newPrice * days}€</Text>
    </Pressable>
  );
};

export default Post;
