import React from 'react';
import {View, Pressable, Text} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './styles';

const SuggestionRow = ({item}) => (
  <View>
    <View style={styles.row}>
      <View style={styles.iconContainer}>
        <Ionicons name="location-sharp" color={'#fff'} size={25} />
      </View>
      <Text style={styles.locationText}>{item.description}</Text>
    </View>
  </View>
);
export default SuggestionRow;
