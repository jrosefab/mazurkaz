import React from 'react';
import {View, Pressable, Text, Image, useWindowDimensions} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';

const PostCarousel = (props) => {
  const post = props.post;
  const navigation = useNavigation();

  const width = useWindowDimensions().width;

  const goToPostPage = () => {
    navigation.navigate('post', {postId: post.id});
  };

  return (
    <Pressable
      onPress={goToPostPage}
      style={[styles.container, {width: width - 60}]}>
      <View style={styles.innerContainer}>
        <Image
          style={styles.image}
          source={{
            uri: post.image,
          }}
        />
        <View style={styles.bedDetails}>
          <Text style={styles.bedrooms}>
            {post.bed} lit {post.bedroom} chambre
          </Text>
          <Text style={styles.description} numberOfLines={2}>
            {post.type}
          </Text>
          <Text style={styles.prices}>
            <Text style={styles.newPrice}>{post.newPrice}€</Text> / nuit
          </Text>
        </View>
      </View>
    </Pressable>
  );
};

export default PostCarousel;
