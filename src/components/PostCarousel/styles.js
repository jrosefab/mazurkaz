import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 120,
    padding: 5,
  },
  innerContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 30,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  image: {
    height: '100%',
    aspectRatio: 1,
    resizeMode: 'cover',
  },
  bedrooms: {
    marginVertical: 5,
    color: '#5b5b5b',
  },
  bedDetails: {
    flex: 1,
    marginHorizontal: 10,
  },
  description: {
    fontSize: 16,
  },
  prices: {
    fontSize: 16,
    marginVertical: 5,
  },
  newPrice: {
    fontWeight: 'bold',
    color: '#000',
  },
});

export default styles;
