import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';
import styles from './styles';

const DetailedPost = (props) => {
  const post = props.post;
  return (
    <ScrollView>
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={{
            uri: post.image,
          }}
        />
        <Text style={styles.bedrooms}>
          {post.bed} lit {post.bedroom} chambre
        </Text>
        <Text style={styles.description} numberOfLines={2}>
          {post.type}
        </Text>
        <Text style={styles.prices}>
          <Text style={styles.oldPrice}>{post.oldPrice}€ </Text>
          <Text style={styles.newPrice}> {post.newPrice}€</Text> / nuit
        </Text>
        <Text style={styles.totalPrice}>Total de {post.totalPrice}€</Text>
        <Text style={styles.longDescription}>{post.longDescription}</Text>
      </View>
    </ScrollView>
  );
};

export default DetailedPost;
