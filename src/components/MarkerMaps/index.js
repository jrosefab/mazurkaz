import React from 'react';
import {View, Text} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import styles from './styles';
import places from '../../../assets/data/resas';

export const MarkerMaps = (props) => {
  const {coordinate, price, onPress, isSelected, type} = props;
  return (
    <Marker onPress={onPress} coordinate={coordinate} title={type}>
      <View
        style={[
          styles.bubble,
          {backgroundColor: isSelected ? '#3BC0AF' : 'gray'},
        ]}>
        <Text style={styles.bubbleText}>{price}€</Text>
      </View>
    </Marker>
  );
};

export default MarkerMaps;
