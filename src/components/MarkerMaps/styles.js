import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  bubble: {
    backgroundColor: '#3BC0AF',
    paddingHorizontal: 20,
    paddingVertical: 10,
    color: '#fff',
    borderRadius: 60,
  },

  bubbleText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default styles;
